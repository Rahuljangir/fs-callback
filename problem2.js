let fs = require('fs');
let path = require('path');

function problem2(lipsumPath) {
    function readLipsumFile(lipsumPath) {
        fs.readFile(lipsumPath, 'utf8', (err, data) => {
            if (err) {
                console.log(err);
            } else {
                console.log("Read the lipsumFile.txt file.")
                wirteInUpperCase(data);
            }
        })
    }
    readLipsumFile(lipsumPath);

    let count = 0
    function writeFileName(fName) {
        let fileName = "fileName.txt";
        let filePath = `./data/${fileName}`;
        let pathOfFile = path.join(__dirname, filePath);
        fs.appendFile(pathOfFile, ' ' + fName, (err) => {
            if (err) {
                console.log(err);
            } else {
                console.log(`file ${fName} written in ${fileName} file`);
            }
        })
        count++;
        if (count === 3) {
            readAndDelete(pathOfFile);
        }
    }

    function wirteInUpperCase(data) {
        let dataInUppercase = data.toUpperCase();
        let fileName = 'upperCase.txt';
        let filePath = `./data/${fileName}`;
        let pathOfFile = path.join(__dirname, filePath);
        fs.writeFile(pathOfFile, dataInUppercase, (err) => {
            if (err) {
                console.log(err);
            } else {
                console.log(`File ${fileName} write successfully`);
            }
        });
        writeFileName(fileName);
        writeInLowerCase(dataInUppercase);
    }

    function writeInLowerCase(data) {
        let dataInLowercase = data.toLowerCase()
            .split('.');
        let sentence = dataInLowercase.join('.\n');
        let fileName = 'lowerCase.txt';
        let filePath = `./data/${fileName}`;
        let pathOfFile = path.join(__dirname, filePath);
        fs.writeFile(pathOfFile, sentence, (err) => {
            if (err) {
                console.log(err);
            } else {
                console.log(`File ${fileName} write successfully`);
            }
        });
        writeFileName(fileName);
        sortTheContent(pathOfFile);
    }

    function sortTheContent(pathOfFile) {
        fs.readFile(pathOfFile, 'utf8', (err, data) => {
            if (err) {
                console.log(err);
            } else {
                console.log("read the new file");
                let sortedData = data.split(' ')
                    .sort().join(' ');
                let fileName = "sortedContent.txt";
                let filePath = `./data/${fileName}`;
                let pathOfFile = path.join(__dirname, filePath);
                fs.writeFile(pathOfFile, sortedData, (err) => {
                    if (err) {
                        console.log(err);
                    } else {
                        console.log(`file ${fileName} write successfully.`)
                    }
                })
                writeFileName(fileName);
            }
        })
    }
    
    function readAndDelete(fPath) {
        fs.readFile(fPath, 'utf8', (err, data) => {
            if (err) {
                console.log(err);
            } else {
                console.log("fileName.txt read successfully");
                let dirPath = fPath.replace('fileName.txt', '');
                let fileName = data.trim()
                    .split(' ');
                let deletedFile = fileName.map((curr) => {
                    fs.unlink(`${dirPath}${curr}`, (err) => {
                        console.log(`${dirPath}${curr}`);
                        if (err) {
                            console.log(err);
                        } else {
                            console.log(`${curr} file deleted successfully`);
                        }
                        return curr;
                    })
                })
            }
        })
    }
}

module.exports = problem2;