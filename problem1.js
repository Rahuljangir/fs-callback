let fs = require('fs');
let path = require('path');

function problem1(directory) {
    fs.mkdir(directory, () => {
        console.log("Directory create successfully")
    });
    let random = Math.floor(Math.random() * 10);
    let index = 0;
    let intervalId = setInterval(() => {
        let fileName = `Hello${index}.JSON`;
        let filePath = `./test/${directory}/${fileName}`;
        let pathOfFile = path.join(__dirname, filePath);
        fs.writeFile(pathOfFile, "Hello World", (err) => {
            if (err) {
                console.log(err);
            } else {
                console.log(`File ${fileName} created successfully`);
            }
        });
        if (index === random) {
            clearInterval(intervalId);
        }
        index++;
    }, 500);
    let count = 0;
    setTimeout(function () {
        let intervalId = setInterval(() => {
            let fileName = `Hello${count}.JSON`;
            let filePath = `./test/${directory}/${fileName}`;
            let pathOfFile = path.join(__dirname, filePath);
            fs.unlink(pathOfFile, (err) => {
                if (err) {
                    console.log(err);
                } else {
                    console.log(`File ${fileName} deleted successfully`);
                }
            });
            if (count === random) {
                clearInterval(intervalId);
            }
            count++;
        }, 500)
    }, 5000)
}

module.exports = problem1